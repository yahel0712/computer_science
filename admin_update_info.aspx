﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin_update_info.aspx.cs" Inherits="Queues_project.admin_update_info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title></title>
    <style>
body {font-family: Arial, Helvetica, sans-serif;}

.navbar {
  width: 100%;
  background-color: #555;
  overflow: auto;
}

.navbar a {
  float: left;
  padding: 12px;
  color: white;
  text-decoration: none;
  font-size: 17px;
}

.navbar a:hover {
  background-color: #000;
}

.active {
  background-color: gray;
}

@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
  }
}
</style>
<style>
        body {
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
            background-color: black;
            overflow: hidden;
            background: url(https://avante.biz/wp-content/uploads/Background-Images-For-Websites/Background-Images-For-Websites-001.jpg);
        }

        * {
            box-sizing: border-box;
        }

        /* Add padding to containers */
        .container {
            text-align:center;
            padding: 16px;
        }
        .infos {
            text-align: center;
            padding: 16px;
            background-color: white;
        }

        /* Full-width input fields */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            display: inline-block;
            border: none;
            background: #f1f1f1;
        }

            input[type=text]:focus, input[type=password]:focus {
                background-color: #ddd;
                outline: none;
            }

        /* Overwrite default styles of hr */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Set a style for the submit button */
        .registerbtn {
            background-color: dodgerblue;
            color: white;
            padding: 16px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
        }

            .registerbtn:hover {
                opacity: 1;
            }

        /* Add a blue text color to links */
        a {
            color: dodgerblue;
        }

        /* Set a grey background color and center the text of the "sign in" section */
        .signin {
            background-color: #f1f1f1;
            text-align: center;
        }
    </style>
</head>

<body>
            <div class="navbar">
    <a class="active" href="admins_page.aspx"><i class="fa fa-fw fa-home"></i> Home</a> 
</div>

    <form runat="server">
        <div class="container">
            <h1>Update info</h1>
            <p>Please fill in this form to update the account info.</p>
            <hr>
            <label for="mail"><b>Enter your mail:</b></label>
            <input type="text" placeholder="Enter mail" name="mail" pattern="[a-zA-Z0-9]{*}@[a-z]{*}.[a-z]{*}" id="mail" required>

            <label for="name"><b>Enter new username:</b></label>
            <input type="text" placeholder="Name" name="name" id="name" required>

            <label for="psw"><b>Enter new password:</b></label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw" required>


            <br />
            <label><b >New gender:</b></label>
            <input type="radio" name="gender" id="genderM" value="male" checked="checked" />Male
            <input type="radio" name="gender" id="genderW" value="female" checked="checked" />Female
            
            <button type="submit" name="submit" id="submit" class="registerbtn" value="send">Submit</button>

        </div>

    </form>

</body>
</html>
